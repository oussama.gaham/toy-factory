public interface AbstractFactory {
	AbstractFactory create(String type);
}
