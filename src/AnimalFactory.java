public class AnimalFactory implements AbstractFactory{
	@Override
	public AbstractFactory create(String type) {
		try {
			Class.forName(type).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
