public class Chien implements Animal{
	String name;

	public Chien(String name) {
		this.name = name;
	}

	@Override
	public String getAnimal() {
		return name;
	}

	@Override
	public String MakeSound() {
		return "Bark...";
	}
}
